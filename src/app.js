const express = require('express');
const app = express();
require("./db/conn");
const port = process.env.PORT || 8000;
const studentRouter = require('./routers/students');
const booksRouter = require('./routers/books');
const personsRouter = require('./routers/persons');

const expressValidator = require('express-validator');
const session = require('express-session');
const MongoDBSession = require('connect-mongodb-session');
const MongoStore = require('connect-mongo');
var cors = require('cors');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(cors());

/** Used for create a new students */
// app.post("/students", (req, res) => {
//     console.log(req.body);
//     const user = new Student(req.body);

//     user.save().then(() => {
//         res.status(200).send(user);
//     }).catch((e) => {
//         res.status(400).send(e);
//     });    
// });

// app.use(studentRouter);
// app.use(booksRouter);
// app.use(personsRouter);

// Express Session Middleware

const store = MongoStore.create({
  mongoUrl: 'mongodb://localhost:27017/graphql',
  collection: 'mySessions'
});

app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false,
    store: store,
}));

// Express Messages Middleware
app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

// Express Validator Middleware
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
      var namespace = param.split('.')
        , root = namespace.shift()
        , formParam = root;
  
      while (namespace.length) {
        formParam += '[' + namespace.shift() + ']';
      }
      return {
        param: formParam,
        msg: msg,
        value: value
      };
    }
}));

const isAuth = (req, res, next) => {
  if (req.session.isAuth) {
    next();
  }
}

app.get("/", (req, res) => {
    // req.session.isAuth = true;
    // console.log(req.session.id);
    res.send('Running')
});

app.get('*', function (req, res, next) {
    res.locals.user = req.user || null;
    next();
});

app.listen(port, () => {
    console.log('listing on port 8000');
});