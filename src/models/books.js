const mongoose = require('mongoose');

const booksSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    genre: {
        type: String,
    },
    authorId: mongoose.Schema.Types.ObjectId,    
});

const Books = new mongoose.model('Book', booksSchema)

module.exports = Books;