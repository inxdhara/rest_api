const mongoose = require('mongoose');

const personsSchema = new mongoose.Schema({
    index: {
        type: Number,
    },
    name: {
        type: String,
    },
    isActive: {
        type: Boolean,
    },
    registered: {
        type: String,
    },
    age: {
        type: Number,
    },
    gender: {
        type: String,
    },
    eyeColor: {
        type: String,
    },
    favoriteFruit: {
        type: String,
    },
    company: {
        type: Object,
    },
    tags: {
        type: Array,
    },
});

const Persons = new mongoose.model('User', personsSchema)

module.exports = Persons;