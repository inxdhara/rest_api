const mongoose = require('mongoose');

const authorSchema = new mongoose.Schema({
    name: {
        type: String,
    },
    age: {
        type: Number,
    },
});

const Authors = new mongoose.model('Author', authorSchema)

module.exports = Authors;