const express = require('express');
const Student = require("./../models/students");
const router = new express.Router();

/** Used for store students data */
router.post("/students", async (req, res) => {
    try{
        const user = new Student(req.body);
        const respone = await user.save();
        req.session.isAuth = true;
        res.status(201).send(respone);
    }catch(e) {
        res.status(400).send(e);
    }
});

/** Used for get students data */
router.get("/students", async (req, res) => {
    try{
        const studentData = await Student.find();
        res.send(studentData);
    }catch(e) {
        res.status(400).send(e);
    }
});

/** Used for update students data */
router.patch("/students/:id", async (req, res) => {
    try{
        const _id = req.params.id;
        const studentData = await Student.findByIdAndUpdate(_id, req.body, {
            new: true
        });
        res.send(studentData);
    }catch(e) {
        res.status(400).send(e);
    }
});

/** Used for delete students data */
router.delete("/students/:id", async (req, res) => {
    try{
        const _id = req.params.id;        
        const studentData = await Student.findByIdAndDelete(_id);
        if (!_id) {
            return res.status(400).send();
        }
        res.send(studentData);
    }catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;