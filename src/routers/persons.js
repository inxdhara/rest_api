const express = require('express');
const Persons = require("./../models/persons");
const router = new express.Router();

/** Used for get persons data */
router.get("/persons", async (req, res) => {
    try{
        // Get all list
        // const personsData = await Persons.find();        

        // Get list group by age
        // const personsData = await Persons.aggregate([
        //     {
        //         $match: {favoriteFruit: "banana"}
        //     },
        //     {
        //         // $group: {_id: "$age"}
        //         // $group: {_id: "$company.location.country"}
        //         // $group: {_id: {age: "$age", gender: "$gender"} }
        //         $group: {_id: {age: "$age", eyeColor: "$eyeColor"} }
        //     },
        // ])

        // const personsData = await Persons.aggregate([
        //     {
        //         // $group: {_id: "$age"}
        //         // $group: {_id: "$company.location.country"}
        //         // $group: {_id: {age: "$age", gender: "$gender"} }
        //         $group: {_id: {age: "$age", eyeColor: "$eyeColor"} }
        //     },
        //     {
        //         $match: { "_id.age": {$gt: 30} }
        //     },
        // ])

        // const personsData = await Persons.aggregate([]).toArray().length;
        // const personsData = await Persons.aggregate([]).itcount;
        // const personsData = await Persons.aggregate([
        //     {
        //         $group: {_id: "$company.location.country"}
        //     },
        //     {
        //         // $count: "allDocumentsCount"
        //         $count: "total"
        //     },
        // ])

        // const personsData = await Persons.aggregate([
        //     {
        //         $limit: 1
        //     },
        //     {
        //         $group: {_id: "$company.location.country"}
        //     },
        //     {
        //         $sort: {_id: -1}
        //     },
        // ])

        const personsData = await Persons.aggregate([
            {
                // $project: {name: 1, "company.location.country": 1}
                $project: {_id:0, name: 1, info: {
                    eyes: "$eyeColor",
                    gender: "$gender"
                }}
            },
        ])

        // const personsData = await Persons.aggregate([
        //     {
        //         $match: {age: {$gt: 25}}
        //     },
        // ])

        res.send(personsData);
    }catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;