const express = require('express');
const Books = require("./../models/books");
const Authors = require("./../models/authors");
const router = new express.Router();

/** Used for get students data */
router.get("/books", async (req, res) => {
    try{
        // const booksData = await Books.find();
        const booksData = await Books.aggregate([
            {
                $lookup: {
                    from: 'authors',
                    localField: 'authorId',
                    foreignField: '_id',
                    as: 'authorDetail',
                },
            },
            {
                $sort: { name: 1 },
            }
        ]);
        res.send(booksData);
    }catch(e) {
        res.status(400).send(e);
    }
});

/** Used for get students data */
router.get("/authors", async (req, res) => {
    try{
        // const authorData = await Authors.find({age: {$gte: 24}}).limit(2);
        // const authorData = await Authors.find({
        //     $and: [
        //         { $or: [{age: 24}, {age: 30}] },
        //         { $or: [{age: 32}, {age: 26}] }
        //     ]
        // });

        const authorData = await Authors.aggregate([
            {
                $group: {_id: "$age"}
            }
        ])

        res.send(authorData);
    }catch(e) {
        res.status(400).send(e);
    }
});

module.exports = router;