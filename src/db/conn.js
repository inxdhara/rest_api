const mongoose = require('mongoose');

// mongoose.connect("mongodb://localhost:27017/students-api", {
mongoose.connect("mongodb://localhost:27017/graphql", {
    useCreateIndex: true, // Set to true to make Mongoose's default index build use createIndex() instead of ensureIndex() to avoid deprecation warnings from the MongoDB driver.
    useNewUrlParser: true, // you must specify a port in your connection string
    useUnifiedTopology: true, // maintaining a stable connection.
    useFindAndModify: false
}).then(() => {
    console.log("connection is successful")
}).catch(() => {
    console.log("No connection")
});